#!/bin/bash
set -euo pipefail

#---------------------------------------------------------------------
# (c) Quercus Technologies (set'2020)
#---------------------------------------------------------------------
#Descr: Runs the BW2 license provider image.
#---------------------------------------------------------------------
Exit_()
{
        echo -e "$1"
        exit 1
}

#---------------------------------------
# V A R I A B L E S    &    C H E C K S
#---------------------------------------
imageName="quercustechnologies/bw2-licenseprovider"
sharedDir=$(git rev-parse --show-toplevel)/shared
defaultImageVersion="latest"
mode="--production"
imageVersion=""
pull=true

while [ $# -gt 0 ]; do
	if [ "$1" == "--develop" ] || [ "$1" == "--production" ]; then
		mode=$1
		shift
	elif [ "$1" == "--local" ]; then
		pull=false
		shift
	elif [ -z "$imageVersion" ] && [ "$1" != "--help" ]; then
		imageVersion=$1
		shift
	else
		[ "$1" != "--help" ] && echo "Invalid parameters"
		echo -e "$(basename $0): [OPTIONS] [image version]"
		echo -e "\t--develop: develop mode"
		echo -e "\t--production: production mode (default)"
		echo -e "\t--local: does not try to pull the image (develop purposes)"
		echo -e "\t- default image version: $defaultImageVersion" 
		[ "$1" != "--help" ] && exit 1
		exit 0
	fi
done

[ -z "$imageVersion" ] && imageVersion="$defaultImageVersion"
[ ! -d $sharedDir ] && mkdir $sharedDir

image="$imageName:$imageVersion"

#---------------------------------------
# M A I N
#---------------------------------------
[ -z $(which docker) ] &&
        Exit_ "Docker is not installed!\nTry running: apt install docker"       

$pull && docker pull "$image"


docker run -it --rm --privileged -v /dev/bus:/dev/bus:ro -v /dev/char:/dev/char:ro -v $sharedDir:/BW/shared "$image" $(hostname) $mode
