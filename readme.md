# Birdwatch 2 License Provider

Tools to build Birdwatch 2 software licenses (startups and upgrades).

## Getting Started

Included tools:

-  `launchBW2LicenseProvider.sh`: license provider launcher.
-  `update.sh`: updates the system to the last version.

### Prerequisites

Docker must be installed. 

```
sudo apt-get install docker
```

## Tools

### BW2 license provider

Run the LPR5 flasher:
```
./launchBW2LicenseProvider.sh
```

BW2 license provider will report the created licenses to the logs directory.
```
./shared/logs/
```

Once created, it will also store the startups and license upgrade files into the repo directory:
```
./shared/repo/
```

For further information about the app:
```
./launchBW2LicenseProvider.sh --help
```

### system update

Use this tool to update the system to the last version.

```
./update.sh
```


## Backups
All data stored into the shared directory MUST be manually backuped up by the user. It's strongly recommended to implement an automatic daily backup for this folder.
```
./shared
```

## Common issues

### launchLPRFlasher / Docker logout

If docker login error: “Error response from daemon: pull access denied for quercustechnologies/...., repository does not exist or may require 'docker login': denied: requested access to the resource is denied”

- Contact with the IT Dpt receive a dockerhub account with have acces permissions to the bw2 license provider repository.
- Login with your dockerhub account
```
docker login
```

## Documentation

* Analysis: webissues #47456
* Developers tips: [dokuwiki](http://r2d2.quercus.biz/dokuwiki/doku.php?id=bam:productiontools)

## Changelog

* #49224: service is able to detect USB dongles connected once it was launched.
* #47456: first release.

## Authors

* **Quercus Technologies - R&D**
