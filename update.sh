#!/bin/bash
#------------------------------------------------------------------
# Quercus Technologies (set'20)
#------------------------------------------------------------------

R="\e[31m"
G="\e[32m"
D="\e[39m"


echo -e $G"Updating... "$D
git pull origin master
if [ $? -eq 0 ]; then
	echo -e $G"Updated."$D
else
	echo -e $R"Can't update."$D
	echo -e $R"If the problem can't be solved, please, remove the current directory and clone the repository again."$D
fi


